@echo off
REM Run this file to build the project outside of the IDE.
REM WARNING: if using a different machine, copy the .rsp files together with this script.
echo i2c_helper.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/i2c_helper.gcc.rsp" || exit 1
echo tmp100na.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/tmp100na.gcc.rsp" || exit 1
echo hc595.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/hc595.gcc.rsp" || exit 1
echo ds3231sn.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/ds3231sn.gcc.rsp" || exit 1
echo bq40z80.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/bq40z80.gcc.rsp" || exit 1
echo w25qXXX.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/w25qXXX.gcc.rsp" || exit 1
echo main.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/main.gcc.rsp" || exit 1
echo gpio.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/gpio.gcc.rsp" || exit 1
echo can.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/can.gcc.rsp" || exit 1
echo dac.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/dac.gcc.rsp" || exit 1
echo i2c.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/i2c.gcc.rsp" || exit 1
echo spi.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/spi.gcc.rsp" || exit 1
echo tim.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/tim.gcc.rsp" || exit 1
echo usart.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/usart.gcc.rsp" || exit 1
echo stm32f1xx_it.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/stm32f1xx_it.gcc.rsp" || exit 1
echo stm32f1xx_hal_msp.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/stm32f1xx_hal_msp.gcc.rsp" || exit 1
echo bq40z80_parser.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/bq40z80_parser.gcc.rsp" || exit 1
echo bq40z80_printer.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/bq40z80_printer.gcc.rsp" || exit 1
echo bq40z80_validator.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/bq40z80_validator.gcc.rsp" || exit 1
echo pretty_printer.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/pretty_printer.gcc.rsp" || exit 1
echo stm32f1xx_hal_gpio_ex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.gcc.rsp" || exit 1
echo stm32f1xx_hal_can.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_can.gcc.rsp" || exit 1
echo stm32f1xx_hal.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.gcc.rsp" || exit 1
echo stm32f1xx_hal_rcc.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.gcc.rsp" || exit 1
echo stm32f1xx_hal_rcc_ex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.gcc.rsp" || exit 1
echo stm32f1xx_hal_gpio.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.gcc.rsp" || exit 1
echo stm32f1xx_hal_dma.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.gcc.rsp" || exit 1
echo stm32f1xx_hal_cortex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.gcc.rsp" || exit 1
echo stm32f1xx_hal_pwr.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.gcc.rsp" || exit 1
echo stm32f1xx_hal_flash.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.gcc.rsp" || exit 1
echo stm32f1xx_hal_flash_ex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.gcc.rsp" || exit 1
echo stm32f1xx_hal_exti.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_exti.gcc.rsp" || exit 1
echo stm32f1xx_hal_dac.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dac.gcc.rsp" || exit 1
echo stm32f1xx_hal_dac_ex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dac_ex.gcc.rsp" || exit 1
echo stm32f1xx_hal_i2c.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_i2c.gcc.rsp" || exit 1
echo stm32f1xx_hal_spi.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.gcc.rsp" || exit 1
echo stm32f1xx_hal_tim.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.gcc.rsp" || exit 1
echo stm32f1xx_hal_tim_ex.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.gcc.rsp" || exit 1
echo stm32f1xx_hal_uart.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.gcc.rsp" || exit 1
echo system_stm32f1xx.c
D:\Keil_v5\ARM\ARMCLANG\bin\armclang.exe @"VisualGDB/Debug/_1_/Core/Src/system_stm32f1xx.gcc.rsp" || exit 1
