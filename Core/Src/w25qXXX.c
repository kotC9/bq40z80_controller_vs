#include "w25qXXX.h"
//---------------------------------------------------------------------------------------------------------------------
#define SPI_TIMEOUT 100
#define _W25_CS(set) HAL_GPIO_WritePin(MEM_nCS_GPIO_Port, MEM_nCS_Pin, set)
#define _W25_HOLD(set) HAL_GPIO_WritePin(MEM_HOLD_GPIO_Port, MEM_HOLD_Pin, set)
//---------------------------------------------------------------------------------------------------------------------
//����� ������������ ��������
#ifndef FREERTOS_CONFIG_H
#define _W25Delay(ms) HAL_Delay(ms)
#else
#define _W25Delay(ms) osDelay(ms)
#endif
//---------------------------------------------------------------------------------------------------------------------
static SPI_HandleTypeDef *w25Spi;
//---------------------------------------------------------------------------------------------------------------------
void w25Init(SPI_HandleTypeDef *hspi)
{
	w25Spi = hspi;
	_W25_CS(GPIO_PIN_SET);
	_W25_HOLD(GPIO_PIN_SET);
}
//---------------------------------------------------------------------------------------------------------------------
inline static void w25spiCmd (uint8_t dataTx)
{
	uint8_t dataRx = 0;
	HAL_SPI_TransmitReceive(w25Spi, &dataTx, &dataRx, 1, SPI_TIMEOUT);
}
//---------------------------------------------------------------------------------------------------------------------
static void w25spi (uint8_t *dataTx, uint8_t *dataRx, uint8_t sizeOfMassive)
{
	_W25_CS(GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(w25Spi, dataTx, dataRx, sizeOfMassive, SPI_TIMEOUT);
	_W25_CS(GPIO_PIN_SET);
}
//---------------------------------------------------------------------------------------------------------------------
uint32_t w25readId (void)
{
	uint8_t w25tempTx[4] = {W25_READ_ID,  0,  0, 0};
	uint8_t w25tempRx[4] = {0,  0,  0, 0};
	w25spi(w25tempTx, w25tempRx, 4);
	return (((uint32_t)w25tempRx[1]) << 16) | (((uint32_t)w25tempRx[2]) << 16) | (uint32_t)w25tempRx[3];
}
//---------------------------------------------------------------------------------------------------------------------
void w25WriteEnable(void)
{
	hc595_set_pin(pin_memUnlockWrite, setOn);
	hc595_update_state();
	_W25_CS(GPIO_PIN_RESET);
	w25spiCmd(W25_WRITE_ENABLE);
	_W25_CS(GPIO_PIN_SET);
}
//---------------------------------------------------------------------------------------------------------------------
void w25WriteDisable(void)
{
	hc595_set_pin(pin_memUnlockWrite, setOff);
	hc595_update_state();
	_W25_CS(GPIO_PIN_RESET);
	w25spiCmd(W25_WRITE_DISABLE);
	_W25_CS(GPIO_PIN_SET);
}
//---------------------------------------------------------------------------------------------------------------------
uint8_t w25ReadStatusRegister(w25StatusReg reg)
{	
	uint8_t w25tempTx[2] = {reg + 0x04,  0};	
	uint8_t w25tempRx[2] = {0,  0};
	w25spi(w25tempTx, w25tempRx, 2);
	return w25tempRx[1];
}
//---------------------------------------------------------------------------------------------------------------------
void w25WriteStatusRegister(uint8_t reg, uint8_t data)
{
	uint8_t w25tempTx[2] = {reg,  data};	
	uint8_t w25tempRx[2] = {0,  0};
	w25spi(w25tempTx, w25tempRx, 2);
}
//---------------------------------------------------------------------------------------------------------------------
void w25WaitEndOfWrite (void)
{
	uint8_t w25temp = 1;
	while(w25temp)
	{
		w25temp = (w25ReadStatusRegister(register1) & 0x01);
		_W25Delay(1);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void w25ChipErase(void)
{
	w25WriteEnable();
	_W25_CS(GPIO_PIN_RESET);
	w25spiCmd(W25_CHIP_ERASE);
	_W25_CS(GPIO_PIN_SET);
	w25WaitEndOfWrite();
	w25WriteDisable();
}
//---------------------------------------------------------------------------------------------------------------------
void  w25WriteMassive (uint8_t *dataTx, uint32_t addressInMem, uint8_t sizeOfMassive)
{
	w25WaitEndOfWrite();
	uint8_t w25tempRx[sizeOfMassive];
	w25WriteEnable();	
	_W25_CS(GPIO_PIN_RESET);
	w25spiCmd(W25_PAGE_PROGRAM);
	w25spiCmd((addressInMem & 0xff000000) >> 24);
	w25spiCmd((addressInMem & 0xff0000) >> 16);
	w25spiCmd((addressInMem & 0xff00) >> 8);
	w25spiCmd(addressInMem & 0xff);
	w25spi(dataTx, w25tempRx, sizeOfMassive);
	w25WaitEndOfWrite();
	w25WriteDisable();
}
//---------------------------------------------------------------------------------------------------------------------
void w25ReadMassive (uint8_t *dataRx, uint32_t addressInMem, uint8_t sizeOfMassive)
{
	uint8_t w25tempTx[sizeOfMassive];	
	_W25_CS(GPIO_PIN_RESET);
	w25spiCmd(W25_READ_DATA);
	w25spiCmd((addressInMem & 0xff000000) >> 24);
	w25spiCmd((addressInMem & 0xff0000) >> 16);
	w25spiCmd((addressInMem & 0xff00) >> 8);
	w25spiCmd(addressInMem & 0xff);
	w25spi(w25tempTx, dataRx, sizeOfMassive);
}

