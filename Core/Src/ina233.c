#include "ina233.h"

I2C_HandleTypeDef *ina233_i2c = NULL;

void INA233_Init(I2C_HandleTypeDef *i2c)
{
    ina233_i2c = i2c;

    unsigned char data[3] = {0xD0, 0x4F, 0x45};
    HAL_I2C_Master_Transmit(ina233_i2c, INA_1 << 1, data, 3, 100); // data is the start pointer of our array

    unsigned char data1[3] = {0xD4, 0x00, 0x14};
    HAL_I2C_Master_Transmit(ina233_i2c, INA_1 << 1, data, 3, 100); // data is the start pointer of our array
}

unsigned short INA233_GetVoltage(unsigned char address)
{
    return I2CHelper_ReadRegisterAsShort(ina233_i2c, address, INA_READ_VIN);
}
unsigned short INA233_GetCurrent(unsigned char address)
{
    return I2CHelper_ReadRegisterAsShort(ina233_i2c, address, INA_READ_IN);
}