#include "hc595.h"
//---------------------------------------------------------------------------------------------------------------------
#define SPI_BUFFER_SIZE 2
#define SPI_TIMEOUT 100
//---------------------------------------------------------------------------------------------------------------------
static SPI_HandleTypeDef *hc595Spi;
static TIM_HandleTypeDef *hc595Tim;
static uint16_t hc595_buff = 0;
//---------------------------------------------------------------------------------------------------------------------
static uint16_t hc595_read_state(void)
{
	uint16_t hc595readBuff = 0;
	//Это можно было сделать изящнее и при этом легко исправимо при смене ног?
	(HAL_GPIO_ReadPin(RG1_QH_CHK_GPIO_Port, RG1_QH_CHK_Pin)) ? (hc595readBuff |= 0x8000) : (hc595readBuff &= ~0x8000);
	(HAL_GPIO_ReadPin(RG1_QG_CHK_GPIO_Port, RG1_QG_CHK_Pin)) ? (hc595readBuff |= 0x4000) : (hc595readBuff &= ~0x4000);
	(HAL_GPIO_ReadPin(RG1_QF_CHK_GPIO_Port, RG1_QF_CHK_Pin)) ? (hc595readBuff |= 0x2000) : (hc595readBuff &= ~0x2000);
	(HAL_GPIO_ReadPin(RG1_QE_CHK_GPIO_Port, RG1_QE_CHK_Pin)) ? (hc595readBuff |= 0x1000) : (hc595readBuff &= ~0x1000);
	(HAL_GPIO_ReadPin(RG1_QD_CHK_GPIO_Port, RG1_QD_CHK_Pin)) ? (hc595readBuff |= 0x800) : (hc595readBuff &= ~0x800);
	(HAL_GPIO_ReadPin(RG1_QC_CHK_GPIO_Port, RG1_QC_CHK_Pin)) ? (hc595readBuff |= 0x400) : (hc595readBuff &= ~0x400);
	(HAL_GPIO_ReadPin(RG1_QB_CHK_GPIO_Port, RG1_QB_CHK_Pin)) ? (hc595readBuff |= 0x200) : (hc595readBuff &= ~0x200);
	(HAL_GPIO_ReadPin(RG1_QA_CHK_GPIO_Port, RG1_QA_CHK_Pin)) ? (hc595readBuff |= 0x100) : (hc595readBuff &= ~0x100);
		
	(HAL_GPIO_ReadPin(RG2_QH_CHK_GPIO_Port, RG2_QH_CHK_Pin)) ? (hc595readBuff |= 0x80) : (hc595readBuff &= ~0x80);
	(HAL_GPIO_ReadPin(RG2_QG_CHK_GPIO_Port, RG2_QG_CHK_Pin)) ? (hc595readBuff |= 0x40) : (hc595readBuff &= ~0x40);
	(HAL_GPIO_ReadPin(RG2_QF_CHK_GPIO_Port, RG2_QF_CHK_Pin)) ? (hc595readBuff |= 0x20) : (hc595readBuff &= ~0x20);
	(HAL_GPIO_ReadPin(RG2_QE_CHK_GPIO_Port, RG2_QE_CHK_Pin)) ? (hc595readBuff |= 0x10) : (hc595readBuff &= ~0x10);
	(HAL_GPIO_ReadPin(RG2_QD_CHK_GPIO_Port, RG2_QD_CHK_Pin)) ? (hc595readBuff |= 0x08) : (hc595readBuff &= ~0x08);
	(HAL_GPIO_ReadPin(RG2_QC_CHK_GPIO_Port, RG2_QC_CHK_Pin)) ? (hc595readBuff |= 0x04) : (hc595readBuff &= ~0x04);
	(HAL_GPIO_ReadPin(RG2_QB_CHK_GPIO_Port, RG2_QB_CHK_Pin)) ? (hc595readBuff |= 0x02) : (hc595readBuff &= ~0x02);
	(HAL_GPIO_ReadPin(RG2_QA_CHK_GPIO_Port, RG2_QA_CHK_Pin)) ? (hc595readBuff |= 0x01) : (hc595readBuff &= ~0x01);
	return hc595readBuff;
}
//---------------------------------------------------------------------------------------------------------------------
static void hc595_write_state(void)
{
	uint8_t sendBuff[2] = { 0, 0 };
	uint8_t receiveBuff[2];
	sendBuff[0] = (uint8_t)(hc595_buff & 0x00ff);
	sendBuff[1] = (uint8_t)(hc595_buff >> 8);
	HAL_SPI_TransmitReceive(hc595Spi, sendBuff, receiveBuff, SPI_BUFFER_SIZE, SPI_TIMEOUT);
	HAL_GPIO_WritePin(RG_CAPTURE_GPIO_Port, RG_CAPTURE_Pin, GPIO_PIN_SET); 	//Дергаем строб
	HAL_GPIO_WritePin(RG_CAPTURE_GPIO_Port, RG_CAPTURE_Pin, GPIO_PIN_SET);  //для небольшой задержки
	HAL_GPIO_WritePin(RG_CAPTURE_GPIO_Port, RG_CAPTURE_Pin, GPIO_PIN_RESET);	
}
//---------------------------------------------------------------------------------------------------------------------
void hc595_set_pin(hc595Pin pinSelect, PinState state)
{
	(state) ? (hc595_buff |= pinSelect) : (hc595_buff &= ~pinSelect);
}
//---------------------------------------------------------------------------------------------------------------------
uint16_t hc595_update_state(void)
{
	hc595_write_state();
	uint16_t hc595writeBuff = hc595_buff;
	uint16_t hc595readBuff = hc595_read_state();
	return (hc595writeBuff ^ hc595readBuff); //Если ноль - ожидаемое и считанное значения совпали, все хорошо
}
//---------------------------------------------------------------------------------------------------------------------
void hc595_init(SPI_HandleTypeDef *hspi, TIM_HandleTypeDef *htim)
{
	hc595Tim = htim;
	hc595Spi = hspi;
	hc595_buff = hc595_read_state();
	HAL_TIM_Base_Start_IT(hc595Tim);
}
//---------------------------------------------------------------------------------------------------------------------
//Если собираетесь использовать переполнение таймера в других местах - просто возьмите это с собой туда.
//P.S. hc595Tim заменить на таймер, используемый для cpu_alive. У меня это tim8.
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == hc595Tim)
	{
		HAL_GPIO_TogglePin(INT_ALIVE_P_GPIO_Port, INT_ALIVE_P_Pin);
	}
	else
	{
		__asm__("nop");
	}
}
//---------------------------------------------------------------------------------------------------------------------
