#include "i2c.h"
#include "tmp100na.h"

uint8_t tmp100_get_temp (I2C_HandleTypeDef *hi2c, uint16_t TmpAddress)
{
	uint8_t data [2];
	
	HAL_I2C_Mem_Read(hi2c, TmpAddress, _tmp100na_reg_temperature, I2C_MEMADD_SIZE_8BIT, data, _tmp100na_reg_size_temperature, TmpTimeout);
	if (data[0]>>7)
		data[0] = (~data[1]) + 1;
	return data[0];
}
