#include "pretty_printer.h"
const unsigned int timeout = 1000;
UART_HandleTypeDef *printer_uart = NULL;

const char *bit_rep[16] = {
    [0] = "0000",
    [1] = "0001",
    [2] = "0010",
    [3] = "0011",
    [4] = "0100",
    [5] = "0101",
    [6] = "0110",
    [7] = "0111",
    [8] = "1000",
    [9] = "1001",
    [10] = "1010",
    [11] = "1011",
    [12] = "1100",
    [13] = "1101",
    [14] = "1110",
    [15] = "1111",
};


void Printer_Init(UART_HandleTypeDef *uart)
{
	printer_uart = uart;
}

/**
 * @brief convert uint32_t value to hex, dec or bin
 * @param base base to convert, see BIN DEC HEX in pretty_printer.h
 * @param valueBuffer used only when converting to binary
 * @param value value to convert
 * @param buf result
 */

static void Printer_ConvertValue(unsigned char base, unsigned char valueBuffer, unsigned int value, unsigned char *buf)
{
	switch (base)
	{
	case DEC:
		sprintf((char *)buf, "%d", value);
		break;
	case HEX:
		sprintf((char *)buf, "0x%x", value);
		break;
	case BIN:
		for (int i = valueBuffer - 1; i >= 0; --i)
		{
			/* convert bits from the end */
			buf[i] = '0' + (value & 1); /* '0' + bit => '0' or '1' */
			value >>= 1;				/* make the next bit the 'low bit' */
		}
		break;
	default:
		sprintf((char *)buf, "%d", value);
		break;
	}
}

/**
 * @brief insert \n to uart
 * @return None 
 */
void Printer_Newline()
{
	uint8_t data[] = "\n";
	HAL_UART_Transmit(printer_uart, data, 1, timeout);
}

/**
 * @brief log char array to uart
 * @param data - data to write
 * @param buffer - data length
 * @return None
 */
void Printer_Log(char *data, unsigned short buffer)
{
	HAL_UART_Transmit(printer_uart, (uint8_t *)data, buffer, timeout);
	Printer_Newline();
}
/**
 * @brief log char * name and char * value
 * 
 * @param name name
 * @param buffer name size
 * @param value char array value
 * @param valueBuffer value size
 */
void Printer_LogValue(char *name, unsigned short buffer, unsigned char *value, unsigned short valueBuffer)
{
	HAL_UART_Transmit(printer_uart, (uint8_t *)name, buffer, timeout);
	HAL_UART_Transmit(printer_uart, value, valueBuffer, timeout);
	Printer_Newline();
}

/**
 * @brief log unsigned char value
 * 
 * @param name name of value
 * @param buffer name size
 * @param value unsigned char value
 * @param base base - BIN, DEC or HEX
 */
void Printer_LogInt8Value(char *name, unsigned short buffer, unsigned char value, unsigned char base)
{
	HAL_UART_Transmit(printer_uart, (uint8_t *)name, buffer, timeout);

	uint8_t buf[8] = {0};
	Printer_ConvertValue(base, 8, value, buf);

	HAL_UART_Transmit(printer_uart, buf, sizeof(buf), timeout);
	Printer_Newline();
}

/**
 * @brief log unsigned short value
 * 
 * @param name name of value
 * @param buffer name size
 * @param value unsigned short value
 * @param base base - BIN, DEC or HEX
 */
void Printer_LogInt16Value(char *name, unsigned short buffer, unsigned short value, unsigned char base)
{
	HAL_UART_Transmit(printer_uart, (uint8_t *)name, buffer, timeout);

	uint8_t buf[16] = {0};
	Printer_ConvertValue(base, 16, value, buf);

	HAL_UART_Transmit(printer_uart, buf, sizeof(buf), timeout);
	Printer_Newline();
}

/**
 * @brief log unsigned int value
 * 
 * @param name name of value
 * @param buffer name size
 * @param value unsigned int value
 * @param base base - BIN, DEC or HEX
 */
void Printer_LogInt32Value(char *name, unsigned short buffer, unsigned int value, unsigned char base)
{
	HAL_UART_Transmit(printer_uart, (uint8_t *)name, buffer, timeout);

	uint8_t buf[32] = {0};
	Printer_ConvertValue(base, 32, value, buf);

	HAL_UART_Transmit(printer_uart, buf, sizeof(buf), timeout);
	Printer_Newline();
}