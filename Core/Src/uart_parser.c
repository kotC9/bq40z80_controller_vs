#include "uart_parser.h"
#include "pretty_printer.h"
#include "bq40z80.h"
#include "bq40z80_action.h"
#include "hc595.h"
#include "ina233.h"
UART_HandleTypeDef *parser_uart = NULL;

/**
 * @brief reset
 */
void SoftReset()
{
    Printer_Log("Restart", 7);
    //__set_FAULTMASK(1);
    NVIC_SystemReset();
}

/**
 * @brief read main info about bq40z80 and send via uart
 * 
 */
static void UartParser_ActionRead()
{
    Printer_Log("t", 1);
    BQAction_UpdateData();
    BQPrinter_ShowAllFlags();
    BQPrinter_ShowMainInfo();
    BQPrinter_ShowFlash();

    Printer_LogInt16Value("ina233 0x40 volt: ", 18, INA233_GetVoltage(0x40), DEC);
    Printer_LogInt16Value("ina233 0x42 volt: ", 18, INA233_GetVoltage(0x42), DEC);
    Printer_LogInt16Value("ina233 0x40 curr: ", 18, INA233_GetCurrent(0x40), DEC);
    Printer_LogInt16Value("ina233 0x42 curr: ", 18, INA233_GetCurrent(0x42), DEC);
    Printer_Log("z", 1);
}
/**
 * @brief unsealed device action and read main info
 * 
 */
static void UartParser_ActionUnsealed()
{
    BQAction_TryUnsealedDevice();
    UartParser_ActionRead();
}
/**
 * @brief write command to bq40z80 and read main info
 * 
 * @param raw uart received array
 */
static void UartParser_ActionCommand(unsigned char *raw)
{
    unsigned short command = raw[1] | (raw[2] << 8); // little endian
    BQ_WriteMABlockCommand(command);
    UartParser_ActionRead();
}
/**
 * @brief write data to flash
 * 
 * @param raw uart received array
 */
static void UartParser_ActionFlash(unsigned char *raw)
{
    unsigned short address = raw[1] | (raw[2] << 8); // little endian
    // 1 byte value
    if (raw[4] == 0x00)
        BQ_WriteFlashByte(address, raw[3]);
    else
        BQ_WriteFlash(address, raw[3] | (raw[4] << 8)); // little endian
    
    // little delay
    HAL_Delay(200);
}
/**
 * @brief uart charge control 
 * 
 * @param raw uart received array
 */
void UartParser_ActionControlBqCharge(unsigned char *raw)
{
    switch (raw[1])
    {
    case 'c':
        if (BQAction_EnableCharging())
            Printer_Log("[BQ] turned on charging", 23);
        else
            Printer_Log("[BQ] failed to turn on charging", 31);
        break;
    case 'd':
        if (BQAction_EnableDischarging())
            Printer_Log("[BQ] turned on discharging", 26);
        else
            Printer_Log("[BQ] failed to turn on discharging", 34);
        break;
    case 'p':
        if (BQAction_EnablePreDischarging())
            Printer_Log("[BQ] turned on pre-discharging", 30);
        else
            Printer_Log("[BQ] failed to turn on pre-discharging", 38);
        break;
    default:
        if (BQAction_DisableFets())
            Printer_Log("[BQ] fet disabled", 17);
        else
            Printer_Log("[BQ] failed to disable fets", 27);
        break;
    }
}
/**
 * @brief kb charge control
 * 
 * @param raw uart received array
 */
static void UartParser_ActionControlKbCharge(unsigned char *raw)
{
    if (raw[1] == 'e')
        hc595_set_pin(pin_chgEn, setOn);
    else
        hc595_set_pin(pin_chgEn, setOff);

    hc595_update_state();
    SoftReset();
}

void UartParser_Init(UART_HandleTypeDef *uart)
{
    parser_uart = uart;
}

void UartParser_Parse(unsigned char *buf)
{
    switch (buf[0])
    {
    case 'r':
        UartParser_ActionRead();
        break;
    case 'm':
        UartParser_ActionCommand(buf);
        break;
    case 'u':
        UartParser_ActionUnsealed();
        break;
    case 'b':
        UartParser_ActionControlBqCharge(buf);
        break;
    case 'k':
        UartParser_ActionControlKbCharge(buf);
        break;
    case 'f':
        UartParser_ActionFlash(buf);
        break;
    default:
        Printer_Log("invalid", 7);
        break;
    }
}