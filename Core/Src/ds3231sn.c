#include "ds3231sn.h"
//---------------------------------------------------------------------------------------------------------------------
static I2C_HandleTypeDef *ds3231snI2c;
//---------------------------------------------------------------------------------------------------------------------
inline static void ds3231_read_reg (uint8_t startReg, uint8_t *Data, uint8_t size)
{
	HAL_I2C_Mem_Read(ds3231snI2c, _addrDs3231sn, startReg, I2C_MEMADD_SIZE_8BIT, Data, size, dsTimeout);
}
//---------------------------------------------------------------------------------------------------------------------
inline static void ds3231_write_reg (uint8_t startReg, uint8_t *Data, uint8_t size)
{
	HAL_I2C_Mem_Write(ds3231snI2c, _addrDs3231sn, startReg, I2C_MEMADD_SIZE_8BIT, Data, size, dsTimeout);
}
//---------------------------------------------------------------------------------------------------------------------
static uint8_t ds3231_B2D(uint8_t bcd)
{
  return (bcd >> 4) * 10 + (bcd & 0x0F);
}
//---------------------------------------------------------------------------------------------------------------------
static uint8_t ds3231_D2B(uint8_t decimal)
{
  return (((decimal / 10) << 4) | (decimal % 10));
}
//---------------------------------------------------------------------------------------------------------------------	
void ds3231snInit (I2C_HandleTypeDef *hi2c)
{
	ds3231snI2c = hi2c;	
}
//---------------------------------------------------------------------------------------------------------------------
void ds3231snGetTime (DS3231SN_RTC_TimeTypeDef *sTime)
{
	uint8_t ds3231_buff[7] = {0, 0, 0, 0, 0, 0, 0};
	ds3231_read_reg(_ds3231_reg_sec, ds3231_buff, 7);
	
	sTime->Seconds = ds3231_B2D(ds3231_buff[0] & 0x7F);
	sTime->Minutes = ds3231_B2D(ds3231_buff[1] & 0x7F);
	sTime->Hours = ds3231_B2D(ds3231_buff[2] & 0x3F);
	sTime->Day = ds3231_B2D(ds3231_buff[3] & 0x07);
	sTime->Date = ds3231_B2D(ds3231_buff[4] & 0x3F);
	sTime->Month = ds3231_B2D(ds3231_buff[5] & 0x1F);
	sTime->Year = ds3231_B2D(ds3231_buff[6]);
}
//---------------------------------------------------------------------------------------------------------------------
void ds3231snSetTime (DS3231SN_RTC_TimeTypeDef *sTime)
{
	uint8_t ds3231_buff[7] = {
															ds3231_D2B(sTime->Seconds), 
															ds3231_D2B(sTime->Minutes), 
															ds3231_D2B(sTime->Hours), 
															ds3231_D2B(sTime->Day), 
															ds3231_D2B(sTime->Date), 
															ds3231_D2B(sTime->Month), 
															ds3231_D2B(sTime->Year)
														};
	ds3231_write_reg(_ds3231_reg_sec, ds3231_buff, 7);
}
//---------------------------------------------------------------------------------------------------------------------
uint16_t DS3231_ReadTemperature(void)
{
	uint8_t ds3231_buff[2] = {0, 0};
	ds3231_read_reg(_ds3231_reg_temp_lsb, ds3231_buff, 2);

  int16_t value = (ds3231_buff[0] << 8) | (ds3231_buff[1]);
  value = (value >> 6);
  return value >> 2;
}
