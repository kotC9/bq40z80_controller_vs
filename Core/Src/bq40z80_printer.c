#include "bq40z80_printer.h"
#include "bq40z80.h"
/** 
 * @attention bq40z80_printer gets data from bq40z80_parser!
 * you must update the data first in bq40z80_parser using
 */
void BQPrinter_ShowMainInfo()
{
	unsigned short temperature = (I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, REG_TEMPERATURE) / 10 - 272.15);
	unsigned short inputVoltage = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, REG_VOLTAGE);
	unsigned short remainingCapacity = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x0F);
	unsigned short fullCapacity = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x10);
	unsigned short cellVoltage = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x3F);
	unsigned short atRateOk = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x07);
	unsigned short atRateTimeToFull = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x05);
	unsigned short current = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x0A);
	unsigned char relativeStateOfCharge = I2CHelper_ReadRegisterAsChar(bq_i2c, bq_deviceAddress, 0x0D);
	unsigned char absoluteStateOfCharge = I2CHelper_ReadRegisterAsChar(bq_i2c, bq_deviceAddress, 0x0E);

	unsigned short averageTimeToEmpty = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x12);
	unsigned short runTimeToEmpty = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x11);
	unsigned short averageTimeToFull = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x13);

	unsigned short serialNumber = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x1C);
	unsigned short chargingCurrent = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x14);
	unsigned short chargingVoltage = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x15);

	unsigned short cycleCount = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x17);
	unsigned short designCapacity = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x18);
	unsigned short designVoltage = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x19);
	unsigned short stateOfHealth = I2CHelper_ReadRegisterAsShort(bq_i2c, bq_deviceAddress, 0x4F);

	unsigned char temp[5];
	I2CHelper_ReadRegister(bq_i2c, bq_deviceAddress, 0x22, temp, 5);

	Printer_LogInt16Value("Temperature:           ", 23, temperature, DEC);
	Printer_LogInt16Value("Input voltage:         ", 23, inputVoltage, DEC);
	Printer_LogInt16Value("Remaining capacity:    ", 23, remainingCapacity, DEC);
	Printer_LogInt16Value("Full charge capacity:  ", 23, fullCapacity, DEC);
	Printer_LogInt16Value("Cell voltage:          ", 23, cellVoltage, DEC);
	Printer_LogInt16Value("Rate ok:               ", 23, atRateOk, DEC);
	Printer_LogInt16Value("Rate full time:        ", 23, atRateTimeToFull, DEC); // 65535 indicates not being charged.
	Printer_LogInt8Value("RelativeStateCharge:    ", 23, relativeStateOfCharge, DEC);
	Printer_LogInt8Value("AbsoluteStateCharge:    ", 23, absoluteStateOfCharge, DEC);

	Printer_LogInt16Value("AverageTime ToEmpty:   ", 23, averageTimeToEmpty, DEC);
	Printer_LogInt16Value("AverageTime ToFull:    ", 23, averageTimeToFull, DEC);
	Printer_LogInt16Value("RunTime To Empty:      ", 23, runTimeToEmpty, DEC);

	Printer_LogInt16Value("Current:               ", 23, current > 10000 ? 65535 - current : current, DEC);
	Printer_LogInt16Value("Serial number:         ", 23, serialNumber, DEC);
	Printer_LogInt16Value("Charging Current:      ", 23, chargingCurrent, DEC);
	Printer_LogInt16Value("Charging Voltage:      ", 23, chargingVoltage, DEC);

	Printer_LogInt16Value("cycle Count:           ", 23, cycleCount, DEC);
	Printer_LogInt16Value("design Capacity:       ", 23, designCapacity, DEC);
	Printer_LogInt16Value("design Voltage:        ", 23, designVoltage, DEC);
	Printer_LogInt16Value("stateOfHealth:         ", 23, stateOfHealth, HEX);
	Printer_LogValue("Chemistry: ", 11, temp, 5);

	unsigned int pfAlert = BQ_ReadCommandAsInt(0x0052);
	unsigned int pfStatus = BQ_ReadCommandAsInt(0x0053);
	unsigned int safetyAlert = BQ_ReadCommandAsInt(0x0050);
	unsigned int safetyStatus = BQ_ReadCommandAsInt(0x0051);

	Printer_LogInt32Value("pfAlert Raw:      ", 18, pfAlert, BIN);
	Printer_LogInt32Value("pfStatus Raw:     ", 18, pfStatus, BIN);
	Printer_LogInt32Value("safetyAlert  Raw: ", 18, safetyAlert, BIN);
	Printer_LogInt32Value("safetyStatus Raw: ", 18, safetyStatus, BIN);

	CHARGE_MODE mode = BQ_GetChargeMode();
	if (mode == CHARGE)
		Printer_Log("Status: CHARGE", 14);
	else if (mode == DISCHARGE)
		Printer_Log("Status: DISCHARGE", 17);
	else
		Printer_Log("Status: RELAX", 13);

	Printer_Log("----- 0xf081 -----", 18);
	Printer_LogInt16Value("curr:  ", 7, BQ_outCal[2] | (BQ_outCal[3] << 8), DEC);
	Printer_LogInt16Value("cell1: ", 7, BQ_outCal[4] | (BQ_outCal[5] << 8), DEC);
	Printer_LogInt16Value("cell2: ", 7, BQ_outCal[6] | (BQ_outCal[7] << 8), DEC);
	Printer_LogInt16Value("cell3: ", 7, BQ_outCal[8] | (BQ_outCal[9] << 8), DEC);
	Printer_LogInt16Value("cell4: ", 7, BQ_outCal[10] | (BQ_outCal[11] << 8), DEC);
	Printer_LogInt16Value("cell5: ", 7, BQ_outCal[12] | (BQ_outCal[13] << 8), DEC);
	Printer_LogInt16Value("cell6: ", 7, BQ_outCal[14] | (BQ_outCal[15] << 8), DEC);
	Printer_LogInt16Value("pack+: ", 7, BQ_outCal[16] | (BQ_outCal[17] << 8), DEC);
	Printer_LogInt16Value("bat  : ", 7, BQ_outCal[18] | (BQ_outCal[19] << 8), DEC);
	Printer_Log("------------------", 18);

	Printer_Log("----- DaStatus1 -----", 21);
	Printer_LogInt16Value("cell1: ", 7, BQ_daStatus1[0] | (BQ_daStatus1[1] << 8), DEC);
	Printer_LogInt16Value("cell2: ", 7, BQ_daStatus1[2] | (BQ_daStatus1[3] << 8), DEC);
	Printer_LogInt16Value("cell3: ", 7, BQ_daStatus1[4] | (BQ_daStatus1[5] << 8), DEC);
	Printer_LogInt16Value("cell4: ", 7, BQ_daStatus1[6] | (BQ_daStatus1[7] << 8), DEC);
	Printer_LogInt16Value("battv: ", 7, BQ_daStatus1[8] | (BQ_daStatus1[9] << 8), DEC);
	Printer_LogInt16Value("pack+: ", 7, BQ_daStatus1[10] | (BQ_daStatus1[11] << 8), DEC);
	Printer_LogInt16Value("curr1: ", 7, BQ_daStatus1[12] | (BQ_daStatus1[13] << 8), DEC);
	Printer_LogInt16Value("curr2: ", 7, BQ_daStatus1[14] | (BQ_daStatus1[15] << 8), DEC);
	Printer_LogInt16Value("curr3: ", 7, BQ_daStatus1[16] | (BQ_daStatus1[17] << 8), DEC);
	Printer_LogInt16Value("curr4: ", 7, BQ_daStatus1[18] | (BQ_daStatus1[19] << 8), DEC);
	Printer_LogInt16Value("v*cur: ", 7, BQ_daStatus1[28] | (BQ_daStatus1[29] << 8), DEC);
	Printer_Log("---------------------", 21);

	Printer_Log("----- DaStatus2 -----", 21);
	Printer_LogInt16Value("int t: ", 7, BQ_daStatus2[0] | (BQ_daStatus2[1] << 8), DEC);
	Printer_LogInt16Value("ts1 t: ", 7, BQ_daStatus2[2] | (BQ_daStatus2[3] << 8), DEC);
	Printer_LogInt16Value("ts2 t: ", 7, BQ_daStatus2[4] | (BQ_daStatus2[5] << 8), DEC);
	Printer_LogInt16Value("ts3 t: ", 7, BQ_daStatus2[6] | (BQ_daStatus2[7] << 8), DEC);
	Printer_LogInt16Value("ts4 t: ", 7, BQ_daStatus2[8] | (BQ_daStatus2[9] << 8), DEC);
	Printer_LogInt16Value("cellt: ", 7, BQ_daStatus2[10] | (BQ_daStatus2[11] << 8), DEC);
	Printer_LogInt16Value("fet t: ", 7, BQ_daStatus2[12] | (BQ_daStatus2[13] << 8), DEC);
	Printer_LogInt16Value("gaugt: ", 7, BQ_daStatus2[14] | (BQ_daStatus2[15] << 8), DEC);
	Printer_Log("---------------------", 21);

	Printer_Log("----- DaStatus3 -----", 21);
	Printer_LogInt16Value("cell5: ", 7, BQ_daStatus3[0] | (BQ_daStatus3[1] << 8), DEC);
	Printer_LogInt16Value("curr5: ", 7, BQ_daStatus3[2] | (BQ_daStatus3[3] << 8), DEC);
	Printer_LogInt16Value("powe5: ", 7, BQ_daStatus3[4] | (BQ_daStatus3[5] << 8), DEC);
	Printer_LogInt16Value("cell6: ", 7, BQ_daStatus3[6] | (BQ_daStatus3[7] << 8), DEC);
	Printer_LogInt16Value("curr6: ", 7, BQ_daStatus3[8] | (BQ_daStatus3[9] << 8), DEC);
	Printer_LogInt16Value("powe5: ", 7, BQ_daStatus3[10] | (BQ_daStatus3[11] << 8), DEC);
	Printer_LogInt16Value("cell7: ", 7, BQ_daStatus3[12] | (BQ_daStatus3[13] << 8), DEC);
	Printer_LogInt16Value("curr7: ", 7, BQ_daStatus3[14] | (BQ_daStatus3[15] << 8), DEC);
	Printer_Log("---------------------", 21);
}

void BQPrinter_ShowFlash()
{
	BQPrinter_ReadFlashGauging();
	BQPrinter_ReadFlashProtections();
	BQPrinter_ReadSettings();
}

void BQPrinter_ReadFlashGauging()
{
	Printer_Log("----- flash gauging -----", 25);
	Printer_LogInt16Value("qmax1:       ", 13, BQ_ReadCommandAsShort(0x44C6), DEC);
	Printer_LogInt16Value("qmax2:       ", 13, BQ_ReadCommandAsShort(0x44C8), DEC);
	Printer_LogInt16Value("qmax3:       ", 13, BQ_ReadCommandAsShort(0x44CA), DEC);
	Printer_LogInt16Value("qmax4:       ", 13, BQ_ReadCommandAsShort(0x44CC), DEC);
	Printer_LogInt16Value("qmax5:       ", 13, BQ_ReadCommandAsShort(0x44CE), DEC);
	Printer_LogInt16Value("qmax6:       ", 13, BQ_ReadCommandAsShort(0x44D0), DEC);
	Printer_LogInt16Value("qmax7:       ", 13, BQ_ReadCommandAsShort(0x44D2), DEC);
	Printer_LogInt16Value("qmaxpack:    ", 13, BQ_ReadCommandAsShort(0x44D4), DEC);

	Printer_LogInt16Value("cell1 eoc:   ", 13, BQ_ReadCommandAsShort(0x44D9), DEC);
	Printer_LogInt16Value("cell2 eoc:   ", 13, BQ_ReadCommandAsShort(0x44DB), DEC);
	Printer_LogInt16Value("cell3 eoc:   ", 13, BQ_ReadCommandAsShort(0x44DD), DEC);
	Printer_LogInt16Value("cell4 eoc:   ", 13, BQ_ReadCommandAsShort(0x44DF), DEC);
	Printer_LogInt16Value("cell5 eoc:   ", 13, BQ_ReadCommandAsShort(0x44E1), DEC);
	Printer_LogInt16Value("cell6 eoc:   ", 13, BQ_ReadCommandAsShort(0x44E3), DEC);
	Printer_LogInt16Value("cell7 eoc:   ", 13, BQ_ReadCommandAsShort(0x44E5), DEC);
	Printer_LogInt16Value("qmaxpack:    ", 13, BQ_ReadCommandAsShort(0x44E7), DEC);
	Printer_Log("-------------------------", 25);
}

void BQPrinter_ReadFlashProtections()
{
	Printer_Log("----- flash protect -----", 25);
	Printer_LogInt8Value("prot conf:         ", 19, BQ_ReadCommandAsShort(0x4BBD), HEX);
	Printer_LogInt8Value("prot A:            ", 19, BQ_ReadCommandAsShort(0x4BBE), HEX);
	Printer_LogInt8Value("prot B:            ", 19, BQ_ReadCommandAsShort(0x4BBF), HEX);
	Printer_LogInt8Value("prot C:            ", 19, BQ_ReadCommandAsShort(0x4BC0), HEX);
	Printer_LogInt8Value("prot D:            ", 19, BQ_ReadCommandAsShort(0x4BC1), HEX);

	Printer_LogInt16Value("cov Low Temp:      ", 19, BQ_ReadCommandAsShort(0x4BCC), DEC);
	Printer_LogInt16Value("cov Standard Low:  ", 19, BQ_ReadCommandAsShort(0x4BCE), DEC);
	Printer_LogInt16Value("cov Standard High: ", 19, BQ_ReadCommandAsShort(0x4BD0), DEC);
	Printer_LogInt16Value("cov High Temp:     ", 19, BQ_ReadCommandAsShort(0x4BD2), DEC);
	Printer_LogInt16Value("cov Rec Temp:      ", 19, BQ_ReadCommandAsShort(0x4BD4), DEC);

	Printer_LogInt16Value("occ1 threshold:    ", 19, BQ_ReadCommandAsShort(0x4BE4), DEC);
	Printer_LogInt16Value("occ2 threshold:    ", 19, BQ_ReadCommandAsShort(0x4BE7), DEC);
	Printer_Log("-------------------------", 25);
}

void BQPrinter_ReadSettings()
{
	Printer_Log("----- flash settings -----", 25);
	Printer_LogInt16Value("fet options:       ", 19, BQ_ReadCommandAsShort(0x4AC7), HEX);
	Printer_LogInt8Value("sbs gaug options:  ", 19, BQ_ReadCommandAsShort(0x4AC9), HEX);
	Printer_LogInt8Value("sbs options:       ", 19, BQ_ReadCommandAsShort(0x4ACA), HEX);
	Printer_LogInt8Value("auth options:      ", 19, BQ_ReadCommandAsShort(0x4ACB), HEX);
	Printer_LogInt16Value("power options:     ", 19, BQ_ReadCommandAsShort(0x4ACC), HEX);
	Printer_LogInt8Value("io options:        ", 19, BQ_ReadCommandAsShort(0x4ACE), HEX);
	Printer_LogInt16Value("pin options:       ", 19, BQ_ReadCommandAsShort(0x4AD3), HEX);
}

/**
 * @brief show all flags
 */
void BQPrinter_ShowAllFlags()
{
	BQPrinter_ShowOperationStatus();
	BQPrinter_ShowChargeStatus();
	BQPrinter_ShowGaugeStatus();
	BQPrinter_ShowBatteryStatus();
	BQPrinter_ShowBatteryMode();
	BQPrinter_ShowManufacturingStatus();
	BQPrinter_ShowGpioStatus();
}

/**
 * @brief function print battery status flags in uart
 */
void BQPrinter_ShowOperationStatus()
{
	Printer_Log("----- OperationStatus -----", 27);
	Printer_LogInt8Value("IATA charge control:              ", 34, BQ_opStatus[31], DEC);
	Printer_LogInt8Value("Emergency FET shutdown:           ", 34, BQ_opStatus[29], DEC);
	Printer_LogInt8Value("Cell balancing status:            ", 34, BQ_opStatus[28], DEC);
	Printer_LogInt8Value("CC measurement in SLEEP mode:     ", 34, BQ_opStatus[27], DEC);
	Printer_LogInt8Value("ADC measurement in SLEEP mode:    ", 34, BQ_opStatus[26], DEC);
	Printer_LogInt8Value("SMBLCAL auto cc starts:           ", 34, BQ_opStatus[25], DEC);
	Printer_LogInt8Value("Initialization after full reset:  ", 34, BQ_opStatus[24], DEC);
	Printer_LogInt8Value("SLEEP mode triggered via command: ", 34, BQ_opStatus[23], DEC);
	Printer_LogInt8Value("400-kHz SMBus mode:               ", 34, BQ_opStatus[22], DEC);
	Printer_LogInt8Value("CAL_OFFSET Calibration out(ccoff) ", 34, BQ_opStatus[21], DEC);
	Printer_LogInt8Value("CAL Calibration out(adc and cc):  ", 34, BQ_opStatus[20], DEC);
	Printer_LogInt8Value("AUTOCALM Auto CC Offset:          ", 34, BQ_opStatus[19], DEC);
	Printer_LogInt8Value("Authentication in progress:       ", 34, BQ_opStatus[18], DEC);
	Printer_LogInt8Value("LED Display:                      ", 34, BQ_opStatus[17], DEC);
	Printer_LogInt8Value("SDM Shutdown triggered:           ", 34, BQ_opStatus[16], DEC);
	Printer_LogInt8Value("SLEEP:                            ", 34, BQ_opStatus[15], DEC);
	Printer_LogInt8Value("XCHG Charging disabled:           ", 34, BQ_opStatus[14], DEC);
	Printer_LogInt8Value("XDSG Discharging disabled:        ", 34, BQ_opStatus[13], DEC);
	Printer_LogInt8Value("PF status:                        ", 34, BQ_opStatus[12], DEC);
	Printer_LogInt8Value("SS Safety status:                 ", 34, BQ_opStatus[11], DEC);
	Printer_LogInt8Value("SDV Shutdown low voltage:         ", 34, BQ_opStatus[10], DEC);
	unsigned char security[2] = {BQ_opStatus[9] + '0', BQ_opStatus[8] + '0'};
	Printer_LogValue("SECURITY mode10-unsealed,01-full: ", 34, security, 2);
	Printer_LogInt8Value("BTP_INT  Battery interrupt:       ", 34, BQ_opStatus[7], DEC);
	Printer_LogInt8Value("Fuse status:                      ", 34, BQ_opStatus[5], DEC);
	Printer_LogInt8Value("PDSG Pre-discharge FET active:    ", 34, BQ_opStatus[4], DEC);
	Printer_LogInt8Value("PCHG Precharge FET active:        ", 34, BQ_opStatus[3], DEC);
	Printer_LogInt8Value("CHG FET active:                   ", 34, BQ_opStatus[2], DEC);
	Printer_LogInt8Value("DSG FET active:                   ", 34, BQ_opStatus[1], DEC);
	Printer_LogInt8Value("PRES System present low:          ", 34, BQ_opStatus[0], DEC);
	Printer_Log("---------------------------", 27);
}

void BQPrinter_ShowChargeStatus()
{
	Printer_Log("----- ChargingStatus -----", 26);
	Printer_LogInt8Value("NCT Near charge termination. :   ", 33, BQ_chargeStatus[19], DEC);
	Printer_LogInt8Value("CCC Charging loss compensation:  ", 33, BQ_chargeStatus[18], DEC);
	Printer_LogInt8Value("CVR Charging voltage rate:       ", 33, BQ_chargeStatus[17], DEC);
	Printer_LogInt8Value("CCR Charging current rate:       ", 33, BQ_chargeStatus[16], DEC);
	Printer_LogInt8Value("VCT Charge termination:          ", 33, BQ_chargeStatus[15], DEC);
	Printer_LogInt8Value("MCHG Maintenance charge:         ", 33, BQ_chargeStatus[14], DEC);
	Printer_LogInt8Value("SU Suspend charge:               ", 33, BQ_chargeStatus[13], DEC);
	Printer_LogInt8Value("IN Charge inhibit:               ", 33, BQ_chargeStatus[12], DEC);

	Printer_LogInt8Value("HV High voltage region:          ", 33, BQ_chargeStatus[11], DEC);
	Printer_LogInt8Value("MV Mid voltage region:           ", 33, BQ_chargeStatus[10], DEC);
	Printer_LogInt8Value("LV Low voltage region:           ", 33, BQ_chargeStatus[9], DEC);
	Printer_LogInt8Value("PV Precharge voltage region:     ", 33, BQ_chargeStatus[8], DEC);

	Printer_LogInt8Value("OT Overtemperature region:       ", 33, BQ_chargeStatus[6], DEC);
	Printer_LogInt8Value("HT High temperature region:      ", 33, BQ_chargeStatus[5], DEC);
	Printer_LogInt8Value("STH Standard temperature high:   ", 33, BQ_chargeStatus[4], DEC);
	Printer_LogInt8Value("RT Recommended temperature reg:  ", 33, BQ_chargeStatus[3], DEC);
	Printer_LogInt8Value("STL Standard temperature low:    ", 33, BQ_chargeStatus[2], DEC);
	Printer_LogInt8Value("LT Low temperature region:       ", 33, BQ_chargeStatus[1], DEC);
	Printer_LogInt8Value("UT Undertemperature region:      ", 33, BQ_chargeStatus[0], DEC);
	Printer_Log("--------------------------", 26);
}

void BQPrinter_ShowGaugeStatus()
{
	Printer_Log("----- Gauge Status -----", 24);
	Printer_LogInt8Value("LOAD constant power:    ", 24, BQ_gaugeStatus[19], DEC);
	Printer_LogInt8Value("SLPQMax ocv update:     ", 24, BQ_gaugeStatus[13], DEC);
	Printer_LogInt8Value("QEN enabled:            ", 24, BQ_gaugeStatus[12], DEC);
	Printer_LogInt8Value("VOK Voltages are OK:    ", 24, BQ_gaugeStatus[11], DEC);
	Printer_LogInt8Value("R_DIS:                  ", 24, BQ_gaugeStatus[10], DEC);
	Printer_LogInt8Value("REST:                   ", 24, BQ_gaugeStatus[8], DEC);
	Printer_LogInt8Value("CF MaxError()>limit:    ", 24, BQ_gaugeStatus[7], DEC);
	Printer_LogInt8Value("DSG Discharge/relax:    ", 24, BQ_gaugeStatus[6], DEC);
	Printer_LogInt8Value("EDV End-of-discharge:   ", 24, BQ_gaugeStatus[5], DEC);
	Printer_LogInt8Value("BAL_EN Balancing:       ", 24, BQ_gaugeStatus[4], DEC);
	Printer_LogInt8Value("TC Terminate charge:    ", 24, BQ_gaugeStatus[3], DEC);
	Printer_LogInt8Value("TD Terminate discharge: ", 24, BQ_gaugeStatus[2], DEC);
	Printer_LogInt8Value("FC Fully charged:       ", 24, BQ_gaugeStatus[1], DEC);
	Printer_LogInt8Value("FD Fully discharged:    ", 24, BQ_gaugeStatus[0], DEC);
	Printer_Log("------------------------", 24);
}

/*
 * function print battery status flags in uart
 */
void BQPrinter_ShowBatteryStatus()
{
	Printer_Log("----- BatteryStatus -----", 25);
	Printer_LogInt8Value("Overcharged Alarm:              ", 32, BQ_batteryStatus[15], DEC);
	Printer_LogInt8Value("Terminate Charge Alarm:         ", 32, BQ_batteryStatus[14], DEC);
	Printer_LogInt8Value("Overtemperature Alarm:          ", 32, BQ_batteryStatus[12], DEC);
	Printer_LogInt8Value("Terminate Discharge Alarm:      ", 32, BQ_batteryStatus[11], DEC);
	Printer_LogInt8Value("Remaining Capacity Alarm:       ", 32, BQ_batteryStatus[9], DEC);
	Printer_LogInt8Value("Remaining Time Alarm:           ", 32, BQ_batteryStatus[8], DEC);
	Printer_LogInt8Value("Initialization:                 ", 32, BQ_batteryStatus[7], DEC);
	Printer_LogInt8Value("Discharging or Relax:           ", 32, BQ_batteryStatus[6], DEC);
	Printer_LogInt8Value("Fully Charged:                  ", 32, BQ_batteryStatus[5], DEC);
	Printer_LogInt8Value("Fully Discharged:               ", 32, BQ_batteryStatus[4], DEC);
	uint8_t temp[4] = {BQ_batteryStatus[0] + '0', BQ_batteryStatus[1] + '0', BQ_batteryStatus[2] + '0', BQ_batteryStatus[3] + '0'};
	Printer_LogValue("Error code 0x0-ok, 0x7-unknown: ", 32, temp, 4);
	Printer_Log("-------------------------", 25);
}

/*
 * function print gpio flags in uart
 */
void BQPrinter_ShowBatteryMode()
{
	//TODO: implement this
}

void BQPrinter_ShowManufacturingStatus()
{
	Printer_Log("----- Manufacturing Status -----", 32);
	Printer_LogInt8Value("CALIBRATION mode:                             ", 46, BQ_manufacturingStatus[15], DEC);
	Printer_LogInt8Value("LIFETIME SPEED UP mode:                       ", 46, BQ_manufacturingStatus[14], DEC);
	Printer_LogInt8Value("PDSG_EN Pre-discharge FET test:               ", 46, BQ_manufacturingStatus[13], DEC);
	Printer_LogInt8Value("LED display is enabled with the push button.: ", 46, BQ_manufacturingStatus[9], DEC);
	Printer_LogInt8Value("FUSE_EN Fuse action:                          ", 46, BQ_manufacturingStatus[8], DEC);
	Printer_LogInt8Value("BBR_EN Black box recorder:                    ", 46, BQ_manufacturingStatus[7], DEC);
	Printer_LogInt8Value("Permanent failure:                            ", 46, BQ_manufacturingStatus[6], DEC);
	Printer_LogInt8Value("LF_EN Lifetime data collection:               ", 46, BQ_manufacturingStatus[5], DEC);
	Printer_LogInt8Value("FET_EN All FET action:                        ", 46, BQ_manufacturingStatus[4], DEC);
	Printer_LogInt8Value("GAUGE_EN Gas gauging:                         ", 46, BQ_manufacturingStatus[3], DEC);
	Printer_LogInt8Value("DSG_EN Discharge FET test:                    ", 46, BQ_manufacturingStatus[2], DEC);
	Printer_LogInt8Value("CHG_EN Charge FET test:                       ", 46, BQ_manufacturingStatus[1], DEC);
	Printer_LogInt8Value("PCHG_EN Precharge FET test:                   ", 46, BQ_manufacturingStatus[0], DEC);
	Printer_Log("--------------------------------", 32);
}

/*
 * function print gpio flags in uart
 * reads wrong(maybe)!! when turned on PIN22, GPIO_READ results that PIN16 turned on
 */
void BQPrinter_ShowGpioStatus()
{
	Printer_Log("----- Gpio -----", 16);
	Printer_LogInt8Value("PIN 22 (LEDCNTLC/GPIO) RL2:                  ", 45, BQ_gpio[7], DEC);
	Printer_LogInt8Value("PIN 21 (LECDNTLB/GPIO) RL1:                  ", 45, BQ_gpio[6], DEC);
	Printer_LogInt8Value("PIN 20 (LEDCNTLA/PDSG/GPIO) RL0:             ", 45, BQ_gpio[5], DEC);
	Printer_LogInt8Value("PIN 17 (PRES/SHUTDN/DISP/GPIO) RH0:          ", 45, BQ_gpio[4], DEC);
	Printer_LogInt8Value("PIN 16 (CB7EN/PDSG/GPIO) RH1:                ", 45, BQ_gpio[3], DEC);
	Printer_LogInt8Value("PIN 15 (VC7EN/DISP/GPIO) RH2:                ", 45, BQ_gpio[2], DEC);
	Printer_LogInt8Value("PIN 13 (DISP/TS4/ADCIN2/GPIO) RC3 or AD3:    ", 45, BQ_gpio[1], DEC);
	Printer_LogInt8Value("PIN 12 (V7SENSE/TS3/ADCIN1/GPIO) RC2 or AD2: ", 45, BQ_gpio[0], DEC);
	Printer_Log("----------------", 16);
}