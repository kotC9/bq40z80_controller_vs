/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stdint.h"
  /* Private includes ----------------------------------------------------------*/
  /* USER CODE BEGIN Includes */

  /* USER CODE END Includes */

  /* Exported types ------------------------------------------------------------*/
  /* USER CODE BEGIN ET */

  /* USER CODE END ET */

  /* Exported constants --------------------------------------------------------*/
  /* USER CODE BEGIN EC */

  /* USER CODE END EC */

  /* Exported macro ------------------------------------------------------------*/
  /* USER CODE BEGIN EM */

  /* USER CODE END EM */

  /* Exported functions prototypes ---------------------------------------------*/
  void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CHG_STAT1_CHK_Pin GPIO_PIN_2
#define CHG_STAT1_CHK_GPIO_Port GPIOE
#define CHG_STAT2_CHK_Pin GPIO_PIN_3
#define CHG_STAT2_CHK_GPIO_Port GPIOE
#define nCHG_PG_CHK_Pin GPIO_PIN_4
#define nCHG_PG_CHK_GPIO_Port GPIOE
#define BTN_SENSE_CHK_Pin GPIO_PIN_5
#define BTN_SENSE_CHK_GPIO_Port GPIOE
#define WDT_RST_Pin GPIO_PIN_1
#define WDT_RST_GPIO_Port GPIOC
#define nM_PCIE_3V3_FLT_Pin GPIO_PIN_2
#define nM_PCIE_3V3_FLT_GPIO_Port GPIOC
#define SOM_POK_CHK_Pin GPIO_PIN_3
#define SOM_POK_CHK_GPIO_Port GPIOC
#define RG1_QA_CHK_Pin GPIO_PIN_0
#define RG1_QA_CHK_GPIO_Port GPIOA
#define RG1_QB_CHK_Pin GPIO_PIN_1
#define RG1_QB_CHK_GPIO_Port GPIOA
#define RG1_QC_CHK_Pin GPIO_PIN_2
#define RG1_QC_CHK_GPIO_Port GPIOA
#define RG1_QD_CHK_Pin GPIO_PIN_3
#define RG1_QD_CHK_GPIO_Port GPIOA
#define MCU_DAC_Pin GPIO_PIN_4
#define MCU_DAC_GPIO_Port GPIOA
#define RG_CAPTURE_Pin GPIO_PIN_4
#define RG_CAPTURE_GPIO_Port GPIOC
#define RG1_QF_CHK_Pin GPIO_PIN_5
#define RG1_QF_CHK_GPIO_Port GPIOC
#define RG1_QG_CHK_Pin GPIO_PIN_0
#define RG1_QG_CHK_GPIO_Port GPIOB
#define RG1_QH_CHK_Pin GPIO_PIN_1
#define RG1_QH_CHK_GPIO_Port GPIOB
#define RG1_QE_CHK_Pin GPIO_PIN_7
#define RG1_QE_CHK_GPIO_Port GPIOE
#define RG2_QD_CHK_Pin GPIO_PIN_8
#define RG2_QD_CHK_GPIO_Port GPIOE
#define RG2_QA_CHK_Pin GPIO_PIN_9
#define RG2_QA_CHK_GPIO_Port GPIOE
#define RG2_QB_CHK_Pin GPIO_PIN_10
#define RG2_QB_CHK_GPIO_Port GPIOE
#define RG2_QC_CHK_Pin GPIO_PIN_11
#define RG2_QC_CHK_GPIO_Port GPIOE
#define RG2_QE_CHK_Pin GPIO_PIN_12
#define RG2_QE_CHK_GPIO_Port GPIOE
#define RG2_QF_CHK_Pin GPIO_PIN_13
#define RG2_QF_CHK_GPIO_Port GPIOE
#define RG2_QG_CHK_Pin GPIO_PIN_14
#define RG2_QG_CHK_GPIO_Port GPIOE
#define RG2_QH_CHK_Pin GPIO_PIN_15
#define RG2_QH_CHK_GPIO_Port GPIOE
#define SPI2_NSS_Pin GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define ACT_LED_Pin GPIO_PIN_10
#define ACT_LED_GPIO_Port GPIOD
#define INT_ALIVE_P_Pin GPIO_PIN_11
#define INT_ALIVE_P_GPIO_Port GPIOD
#define nCPU_OK_CHK_Pin GPIO_PIN_12
#define nCPU_OK_CHK_GPIO_Port GPIOD
#define nM_PCIE_1V5_FLT_Pin GPIO_PIN_13
#define nM_PCIE_1V5_FLT_GPIO_Port GPIOD
#define FP_nOC_CHK_Pin GPIO_PIN_14
#define FP_nOC_CHK_GPIO_Port GPIOD
#define LV_FAN_TACHO_Pin GPIO_PIN_7
#define LV_FAN_TACHO_GPIO_Port GPIOC
#define FAN_ENABLE_Pin GPIO_PIN_8
#define FAN_ENABLE_GPIO_Port GPIOC
#define RCC_MCO_TEST_Pin GPIO_PIN_8
#define RCC_MCO_TEST_GPIO_Port GPIOA
#define MEM_nCS_Pin GPIO_PIN_6
#define MEM_nCS_GPIO_Port GPIOD
#define MEM_HOLD_Pin GPIO_PIN_7
#define MEM_HOLD_GPIO_Port GPIOD
#define MCU_PB3_Pin GPIO_PIN_3
#define MCU_PB3_GPIO_Port GPIOB
#define MCU_PB4_Pin GPIO_PIN_4
#define MCU_PB4_GPIO_Port GPIOB
#define MCU_PB5_Pin GPIO_PIN_5
#define MCU_PB5_GPIO_Port GPIOB
#define POK_5V_CHK_Pin GPIO_PIN_8
#define POK_5V_CHK_GPIO_Port GPIOB
#define POK_SYS_CHK_Pin GPIO_PIN_9
#define POK_SYS_CHK_GPIO_Port GPIOB
#define AUX_nOC_CHK_Pin GPIO_PIN_0
#define AUX_nOC_CHK_GPIO_Port GPIOE
#define SPO2_nOC_CHK_Pin GPIO_PIN_1
#define SPO2_nOC_CHK_GPIO_Port GPIOE

#define i2c_clk_test GPIO_PIN_10
#define i2c_clk_test_GPIO_Port GPIOB
#define i2c_sda_test GPIO_PIN_11
#define i2c_sda_test_GPIO_Port GPIOB
  /* USER CODE BEGIN Private defines */

  /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
