﻿#include "pretty_printer.h"
#include "bq40z80_parser.h"
#include "bq40z80_printer.h"
#include "bq40z80_validator.h"
#include "bq40z80_flash.h"
#include "bq40z80_action.h"
#include "i2c_helper.h"
#include "i2c.h"
#include "gpio.h"
#include "stdio.h"

#define USE_SCANNER 1

#define REG_TEMPERATURE 0x08
#define REG_VOLTAGE 0x09 // input voltage
#define REG_RELATIVE_STATE_CHARGE 0x0D
#define REG_ABSOLUTE_STATE_CHARGE 0x0E
#define REG_FULL_CHARGE_CAPACITY 0x10
#define REG_GPIO_WRITE 0x49
#define REG_GPIO_READ 0x48
#define REG_BATTERY_MODE 0x03
#define REG_BATTERY_STATUS 0x16
#define REG_CYCLE_COUNT 0x17
#define REG_OPERATION_STATUS 0x54
#define REG_PF_STATUS 0x53
#define REG_SAFETY_STATUS 0x51
#define REG_CHARGING_STATUS 0x55
#define REG_GAUGING_STATUS 0x56
#define REG_MANAFACTURING_STATUS 0x57P

extern unsigned char bq_deviceAddress;
extern I2C_HandleTypeDef *bq_i2c;

void BQ_Init(I2C_HandleTypeDef *i2c);

void BQ_WriteMABlockCommand(unsigned short command);
void BQ_ReadMABlockCommand(unsigned short command, unsigned char *receive, unsigned char size);

unsigned short BQ_ReadCommandAsShort(unsigned short command);
unsigned int BQ_ReadCommandAsInt(unsigned short command);
unsigned char BQ_ReadCommandAsChar(unsigned short command);

void BQ_WriteFlash(unsigned short addr, unsigned short writeData);
void BQ_WriteFlashByte(unsigned short addr, unsigned char writeData);