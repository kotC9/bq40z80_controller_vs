#include "usart.h"
#ifndef PRPRINTER_H
#define PRPRINTER_H
#define DEC 10
#define HEX 16
#define BIN 2

extern UART_HandleTypeDef *printer_uart;
void Printer_Init(UART_HandleTypeDef *uart);
void Printer_Log(char *data, unsigned short buffer);
void Printer_LogValue(char *name, unsigned short buffer, unsigned char *value, unsigned short valueBuffer);
void Printer_LogInt8Value(char *name, unsigned short buffer, unsigned char value, unsigned char base);
void Printer_LogInt16Value(char *name, unsigned short buffer, unsigned short value, unsigned char base);
void Printer_LogInt32Value(char *name, unsigned short buffer, unsigned int value, unsigned char base);

#endif