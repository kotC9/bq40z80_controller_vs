#ifndef __DS3231SN_H__
#define __DS3231SN_H__
//I2c
#include "i2c.h"
#define dsTimeout 100
//Addres
#define _addrDs3231sn 0xd0
//Registers
#define _ds3231_reg_sec 0x00
#define _ds3231_reg_min 0x01
#define _ds3231_reg_hour 0x02
#define _ds3231_reg_day 0x03
#define _ds3231_reg_date 0x04
#define _ds3231_reg_month 0x05
#define _ds3231_reg_year 0x06
#define _ds3231_reg_alarm1_sec 0x07
#define _ds3231_reg_alarm1_min 0x08
#define _ds3231_reg_alarm1_hours 0x09
#define _ds3231_reg_alarm1_day 0x0a
#define _ds3231_reg_alarm2_min 0x0b
#define _ds3231_reg_alarm2_hours 0x0c
#define _ds3231_reg_alarm2_day 0x0d
#define _ds3231_reg_control 0x0e
#define _ds3231_reg_status 0x0f
#define _ds3231_reg_ag_offset 0x10
#define _ds3231_reg_temp_msb 0x11
#define _ds3231_reg_temp_lsb 0x12

typedef struct
{
	uint8_t Seconds; //������� 0-59
	uint8_t Minutes; //������ 0-59
	uint8_t Hours;	 //���� 0-59
	uint8_t Day;	 //���� ������ 1-7
	uint8_t Date;	 //���� ������ 1-31
	uint8_t Month;	 //����� 1-12
	uint8_t Year;	 //��� 0-99

} DS3231SN_RTC_TimeTypeDef;

void ds3231snInit(I2C_HandleTypeDef *hi2c);			   //������� �������������. �������� i2c.
void ds3231snGetTime(DS3231SN_RTC_TimeTypeDef *sTime); //������ ��������� � � ������ � ��� ���, �� ������, �� ���
void ds3231snSetTime(DS3231SN_RTC_TimeTypeDef *sTime); //������ ��������� � � ����� � sd3231sn ��� �� ���.
uint16_t ds3231snGetTemp(void);
#endif
