#include "usart.h"
extern UART_HandleTypeDef * parser_uart;

void UartParser_Init(UART_HandleTypeDef * uart);
void UartParser_Parse(unsigned char * raw);