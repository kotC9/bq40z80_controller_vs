#ifndef __MEM25_H__
#define __MEM25_H__

#include "spi.h"
#include "hc595.h"
//---------------------------------------------------------------------------------------------------------------------
//Instruction
#define W25_DUMMY_BYTES 0x00
#define W25_READ_ID 0x9f
#define W25_READ_UNIQ_ID 0x4b
#define W25_WRITE_ENABLE 0x06
#define W25_WRITE_DISABLE 0x04
#define W25_WRITE_DISABLE 0x04
#define W25_VOL_SR_WRITE_EN 0x50
#define W25_RESET_DEVICE 0x99
#define W25_STATUS_REG1_READ 0x05
#define W25_STATUS_REG2_READ 0x35
#define W25_STATUS_REG3_READ 0x15
#define W25_STATUS_REG1_WRITE 0x01
#define W25_STATUS_REG2_WRITE 0x31
#define W25_STATUS_REG3_WRITE 0x11
#define W25_CHIP_ERASE 0xc7
#define W25_PAGE_PROGRAM 0x02
#define W25_READ_DATA 0x03
//---------------------------------------------------------------------------------------------------------------------
typedef enum
{
  register1 = W25_STATUS_REG1_WRITE,
  register2 = W25_STATUS_REG2_WRITE,
  register3 = W25_STATUS_REG3_WRITE
} w25StatusReg;
//---------------------------------------------------------------------------------------------------------------------
//Status register 1 (1 or 5 address) contain: SRP, SEC, TB, BP2, BP1, BP0, WEL, BUSY
//Status register 2 (31 or 35 address) contain: SUS, CMP, LB3, LB2, LB1, (R), QE, SRL
//Status register 3 (11 or 15 address)contain: (R), DRV1, DRV2, (R), (R), WPS, (R), (R)
//SRL - Status Regiset Lock: ���� 1, �� SRP � WP �� �����. ������� �� �����.
//SRP - Status Register Protect: ���� 0, �� WP �� �����. ������� �� �����.
//� ������ ���� SRL = 0 � SRP = 1, �� ��� WP = 1 ������ ���������.
// BUSY - ������/������ � �������� ���� 1, ����  0 - ����� ������/���������� ������.
//---------------------------------------------------------------------------------------------------------------------
//Functions
void w25Init(SPI_HandleTypeDef *hspi);                                               //������ �������������. �������� spi, �� ������� ����� ������
uint32_t w25readId(void);                                                            //������� ��������� ID ������. ��� ����� ������ ���������� 0x00EF4018 (EF - Manufacturier ID, 4018 - W25Q128JV-IQ/JQ)
void w25ReadMassive(uint8_t *dataRx, uint32_t addressInMem, uint8_t sizeOfMassive);  //������� ������ ������� �� ������
void w25ChipErase(void);                                                             //������� �������� ������. ����.
void w25WriteMassive(uint8_t *dataTx, uint32_t addressInMem, uint8_t sizeOfMassive); //������� ���������������� ������� � ������. ����� ����������������� ����� ������� ������.
uint8_t w25ReadStatusRegister(w25StatusReg reg);                                     //������� ������ ��������� ��������. ��������� �� ������ ������.
#endif
