#ifndef __HC595_H__
#define __HC595_H__

//#include "stm32f0xx_hal.h"
#include "spi.h"
#include "tim.h"

typedef enum
{
  setOff  = 0x00,
  setOn = 0x01
} PinState;

typedef enum
{
	pin_soundMute = 0x0080,
	pin_auxPwr = 0x0040,
	pin_spo2Pwr = 0x0020,
	pin_mcuBtnRed = 0x0010,
	pin_mcuBtnGrn = 0x0008,
	pin_chgLowCurr = 0x0004,
	pin_shutdown = 0x0002,
	pin_chgEn = 0x0001,
	
	pin_ext2 = 0x8000,
	pin_ext1 = 0x4000,
	pin_memUnlockWrite = 0x2000,
	pin_pcie1v5 = 0x1000,
	pin_frontPanel = 0x0800,
	pin_somPwr = 0x0400,
	pin_pcie3v3 = 0x0200,
	pin_buzz = 0x0100
} hc595Pin;

void hc595_init (SPI_HandleTypeDef *hspi, TIM_HandleTypeDef *htim); //����� �������� spi � ������, ����� ��������� ��������� ��� � �����
void hc595_set_pin(hc595Pin pinSelect, PinState state); //����� � ������ �������� ��������
uint16_t hc595_update_state (void); //���������� � hc595 ��������� �� ������ � ���������� ��� �������� � ���, ��� ������. ���� 0 - ��� �������.
#endif
