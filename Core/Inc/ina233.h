#include "i2c_helper.h"

#define INA_1 0x40
#define INA_READ_VIN 0x88
#define INA_READ_IN 0x89
#define INA_READ_VOUT 0x8B
#define INA_READ_IOUT 0x8C
#define MFR_READ_VSHUNT 0xD1
#define INA_READ_IOUT 0x8C

extern I2C_HandleTypeDef *ina233_i2c;

void INA233_Init(I2C_HandleTypeDef *i2c);
unsigned short INA233_GetVoltage(unsigned char address);
unsigned short INA233_GetCurrent(unsigned char address);