#include "bq40z80_parser.h"

void BQPrinter_ShowMainInfo();
void BQPrinter_ShowAllFlags();
void BQPrinter_ShowOperationStatus();
void BQPrinter_ShowBatteryMode();
void BQPrinter_ShowBatteryStatus();
void BQPrinter_ShowGpioStatus();
void BQPrinter_ReadFlashGauging();
void BQPrinter_ReadFlashProtections();
void BQPrinter_ReadSettings();
void BQPrinter_ShowFlash();
void BQPrinter_ShowManufacturingStatus();
void BQPrinter_ShowGaugeStatus();
void BQPrinter_ShowChargeStatus();
