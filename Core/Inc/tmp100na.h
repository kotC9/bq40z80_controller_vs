#ifndef __TMP100NA_H__
#define __TMP100NA_H__
#include "i2c.h"

#define TmpTimeout 100

#define _addrTmpAmbientTemp 0x90 //ADD0 = 0, ADD1 = 0
#define _addrTmpPwrOutTemp 0x94 //ADD0 = 1, ADD1 = 0

#define _tmp100na_reg_temperature 0x00
#define _tmp100na_reg_cfg 0x01
#define _tmp100na_reg_temperatureAlarmLow 0x00
#define _tmp100na_reg_temperatureAlarmHi 0x00

#define _tmp100na_reg_size_temperature 2
#define _tmp100na_reg_size_cfg 1
#define _tmp100na_reg_size_temperatureAlarmLow 2
#define _tmp100na_reg_size_temperatureAlarmHi 2

uint8_t tmp100_get_temp (I2C_HandleTypeDef *hi2c, uint16_t TmpAddress);

#endif
